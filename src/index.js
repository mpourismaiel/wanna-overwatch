import React from 'react';
import ReactDOM from 'react-dom';
import Pushe from 'pushe-webpush';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

Pushe.init("qd23x9mvrym7lqle");

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

Pushe.subscribe({
  title: 'خبر اورواچ',
  content: 'میخوای یکی گفت اورواچ بگی ها؟',
  acceptText: 'آها',
  rejectText: 'برو بمیر'
});
