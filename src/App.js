import React from 'react';
import Pushe from 'pushe-webpush';

import './App.css';

function App() {
  const [name, setName] = React.useState('');

  return (
    <div className="Container">
      <input type="text" value={name} onChange={e => setName(e.target.value)} placeholder="اسمتو بگو عمویی" />
      <button className="Button" onClick={() => Pushe.captureEvent('overwatch', `${name || 'someone'} wanna sum overwatch`)}>اورواچ موخوام!</button>
    </div>
  );
}

export default App;
